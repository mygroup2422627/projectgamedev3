using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle4Checker4 : MonoBehaviour
{
    public GameObject Spotlight1;
    public GameObject Spotlight11;
    public GameObject Spotlight2;
    public GameObject Spotlight22;
    public GameObject Spotlight3;
    public GameObject Spotlight33;
    public GameObject Spotlight4;
    public GameObject Spotlight44;

    private bool canInteract = true;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (canInteract && Input.GetKey(KeyCode.E))
            {
                ToggleSpotlights(Spotlight4);
                ToggleSpotlights(Spotlight44);
                canInteract = false;
                StartCoroutine(EnableInteractAfterDelay(0.5f));
            }
        }
    }
    private void ToggleSpotlights(GameObject spotlight)
    {
        if (spotlight != null)
        {
            spotlight.SetActive(!spotlight.activeSelf);
        }
    }
    private IEnumerator EnableInteractAfterDelay(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        canInteract = true;
    }
}