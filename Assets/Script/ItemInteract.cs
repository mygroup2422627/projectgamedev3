using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInteract : MonoBehaviour
{
    public GameObject Door;
    public GameObject Light1;
    public GameObject Light2;
    public GameObject Light3;
    public GameObject Light4;
	public GameObject KhomLoiItem;
	public GameObject ThongItem;
	public GameObject TangpatipItem;
	public GameObject Item1;
	public GameObject Item2;
	public GameObject Item3;
    public GameObject ShadowKhomLoiCheckerItem;
    public GameObject ShadowThongCheckerItem;
    public GameObject ShadowTangpatipCheckerItem;
    void Start()
    {
        Door.SetActive(true);
      	ShadowKhomLoiCheckerItem.SetActive(true);
      	ShadowThongCheckerItem.SetActive(true);
      	ShadowTangpatipCheckerItem.SetActive(true);
    }
	void Update()
	{
		if(KhomLoiItem.activeSelf && ThongItem.activeSelf && TangpatipItem.activeSelf)
		{
			Item1.SetActive(false);
			Item2.SetActive(false);
			Item3.SetActive(false);
    	  	ShadowKhomLoiCheckerItem.SetActive(true);
    	  	ShadowThongCheckerItem.SetActive(true);
    	  	ShadowTangpatipCheckerItem.SetActive(true);
		}
	}
	private void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (Input.GetKey(KeyCode.E))
			{
				Door.SetActive(false);
				Light1.SetActive(false);
				Light2.SetActive(false);
				Light3.SetActive(false);
				Light4.SetActive(false);
				Item1.SetActive(true);
				Item2.SetActive(true);
				Item3.SetActive(true);
				ShadowKhomLoiCheckerItem.SetActive(false);
				ShadowThongCheckerItem.SetActive(false);
				ShadowTangpatipCheckerItem.SetActive(false);
			}
		}
	}
}