using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHand : MonoBehaviour
{
    public GameObject Hands;
    // Start is called before the first frame update
    void Start()
    {
        Hands.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Hands.SetActive(true);
        }
    }
}
