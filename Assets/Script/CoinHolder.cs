using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinHolder : MonoBehaviour
{
    public GameObject Coin;
    public GameObject Krong;
    public GameObject Fuen;
    public GameObject Text;
    
    private bool coinUsed = false;
    void Start()
    {
        Coin.SetActive(false);
        Krong.SetActive(true);
        Text.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!coinUsed)
            {
                Text.SetActive(true);
            }
            else if (coinUsed)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !coinUsed && PlayerHasCoinInInventory())
            {
                Krong.SetActive(false);
                Text.SetActive(false);
                coinUsed = true;
                UseCoin();
                Item coin = null;
                foreach (Item item in InventoryController.Instance.Items)
                {
                    if (item.itemType == Item.ItemType.Coin)
                    {
                        coin = item;
                        break;
                    }
                }
                if (coin != null)
                {
                    InventoryController.Instance.Remove(coin);
                    InventoryController.Instance.SetInventoryItems();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    public void UseCoin()
    {
        Coin.SetActive(true);
        StartCoroutine(WaitAndEnablePickup());
    }

    private IEnumerator WaitAndEnablePickup()
    {
        yield return new WaitForSeconds(0.2f);
        EnableCollider(Fuen);
    }
    private void EnableCollider(GameObject obj)
    {
        Collider collider = obj.GetComponent<Collider>();
        if (collider != null)
        {
            collider.enabled = true;
        }
    }
    private bool PlayerHasCoinInInventory()
    {
        foreach (var item in InventoryController.Instance.Items)
        {
            if (item.itemType == Item.ItemType.Coin)
            {
                return true;
            }
        }
        return false;
    }
}