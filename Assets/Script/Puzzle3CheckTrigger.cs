using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle3CheckTrigger : MonoBehaviour
{
    public GameObject coinToActivate;
    private int collidersCount = 0;
    void Start()
    {
        coinToActivate.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PuppetTrigger"))
        {
            collidersCount++;
            if (collidersCount >= 3)
            {
                coinToActivate.SetActive(true);
            }
        }
    }
    /*void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PuppetTrigger"))
        {
            collidersCount--;
            if (collidersCount < 3)
            {
                coinToActivate.SetActive(false);
            }
        }
    }*/
}