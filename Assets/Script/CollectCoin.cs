using UnityEngine;

public class CollectCoin : MonoBehaviour
{
    public AudioSource coinSound;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Coin")
        {
            if (Input.GetKey(KeyCode.E))
            {
                coinSound.Play();
            }
        }
    }
}
