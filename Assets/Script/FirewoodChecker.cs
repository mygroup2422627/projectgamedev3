using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirewoodChecker : MonoBehaviour
{
    public GameObject Fire;
    public GameObject Text;
    public GameObject Firewood;

    void Start()
    {
        Fire.SetActive(false);
        Firewood.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(!Fire.activeSelf)
            {
                Text.SetActive(true);
            }
            else if (Fire.activeSelf)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !Fire.activeSelf && !Firewood.activeSelf)
            {
                Text.SetActive(false);
                InventoryController inventoryController = InventoryController.Instance;
                if (inventoryController.Items.Exists(item => item.itemType == Item.ItemType.Firewood))
                {
                    Fire.SetActive(true);
                    Firewood.SetActive(true);
                }
                Item firewood = null;
                foreach (Item item in InventoryController.Instance.Items)
                {
                    if (item.itemType == Item.ItemType.Firewood)
                    {
                        firewood = item;
                        break;
                    }
                }
                if (firewood != null)
                {
                    InventoryController.Instance.Remove(firewood);
                    InventoryController.Instance.SetInventoryItems();
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Text.SetActive(false);
    }
}