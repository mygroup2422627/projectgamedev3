using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Puzzle1Checker : MonoBehaviour
{
    public GameObject Coin;
    public GameObject KhomLoi1;
    public GameObject Thong1;
    public GameObject Tangpatip1;
    public GameObject Khom1;
    public GameObject KhomLoi2;
    public GameObject Thong2;
    public GameObject Tangpatip2;
    public GameObject Khom2;
    public GameObject KhomLoi3;
    public GameObject Thong3;
    public GameObject Tangpatip3;
    public GameObject Khom3;
    public GameObject KhomLoiKC;
    public GameObject ThongKC;
    public GameObject TangpatipKC;
    public GameObject KhomKC;
    public GameObject KhomLoiTHC;
    public GameObject ThongTHC;
    public GameObject TangpatipTHC;
    public GameObject KhomTHC;
    public GameObject KhomLoiTC;
    public GameObject ThongTC;
    public GameObject TangpatipTC;
    public GameObject KhomTC;
    public GameObject KCKhomLoiShadowUI;
    public GameObject KCThongShadowUI;
    public GameObject KCTangpatipShadowUI;
    public GameObject KCKhomShadowUI;
    public GameObject THCKhomLoiShadowUI;
    public GameObject THCThongShadowUI;
    public GameObject THCTangpatipShadowUI;
    public GameObject THCKhomShadowUI;
    public GameObject TCKhomLoiShadowUI;
    public GameObject TCThongShadowUI;
    public GameObject TCTangpatipShadowUI;
    public GameObject TCKhomShadowUI;
    public GameObject Text;
 
    private bool KhomLoi1Used = true;
    private bool Thong1Used = true;
    private bool Tangpatip1Used = true;
    private bool Khom1Used = true;
    private bool KhomLoi2Used = true;
    private bool Thong2Used = true;
    private bool Tangpatip2Used = true;
    private bool Khom2Used = true;
    private bool KhomLoi3Used = true;
    private bool Thong3Used = true;
    private bool Tangpatip3Used = true;
    private bool Khom3Used = true;
    private bool hasActivatedCoin = false;

    public Item khomLoiItem;
    public Item khomItem;
    public Item thongItem;
    public Item TangpatipItem;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Text.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                if (!hasActivatedCoin && KhomLoi1 != null && Thong2 != null && Tangpatip3 != null)
                {
                    if (KhomLoi1.activeSelf && Thong2.activeSelf && Tangpatip3.activeSelf)
                    {
                        Coin.SetActive(true);
                        hasActivatedCoin = true;
                    }
                    else
                    {
                        CollectItem();
                        ResetState();
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    private void CollectItem()
    {
        if (KhomLoi1.activeSelf && KhomLoi1Used)
        {
            CollectKhomLoi1();
            KhomLoi1Used = false;
            KhomLoiKC.SetActive(true);
            KhomLoiTHC.SetActive(true);
            KhomLoiTC.SetActive(true);
            KCKhomLoiShadowUI.SetActive(false);
            THCKhomLoiShadowUI.SetActive(false);
            TCKhomLoiShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomLoiItem);
            }
        }
        if (KhomLoi2.activeSelf && KhomLoi2Used)
        {
            CollectKhomLoi2();
            KhomLoi2Used = false;
            KhomLoiKC.SetActive(true);
            KhomLoiTHC.SetActive(true);
            KhomLoiTC.SetActive(true);
            KCKhomLoiShadowUI.SetActive(false);
            THCKhomLoiShadowUI.SetActive(false);
            TCKhomLoiShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomLoiItem);
            }
        }
        if (KhomLoi3.activeSelf && KhomLoi3Used)
        {
            CollectKhomLoi3();
            KhomLoi3Used = false;
            KhomLoiKC.SetActive(true);
            KhomLoiTHC.SetActive(true);
            KhomLoiTC.SetActive(true);
            KCKhomLoiShadowUI.SetActive(false);
            THCKhomLoiShadowUI.SetActive(false);
            TCKhomLoiShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomLoiItem);
            }
        }
        if (Thong1.activeSelf && Thong1Used)
        {
            CollectThong1();
            Thong1Used = false;
            ThongKC.SetActive(true);
            ThongTHC.SetActive(true); 
            ThongTC.SetActive(true);
            KCThongShadowUI.SetActive(false);
            THCThongShadowUI.SetActive(false);
            TCThongShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(thongItem);
            }
        }
        if (Thong2.activeSelf && Thong2Used)
        {
            CollectThong2();
            Thong2Used = false;
            ThongKC.SetActive(true);
            ThongTHC.SetActive(true); 
            ThongTC.SetActive(true);
            KCThongShadowUI.SetActive(false);
            THCThongShadowUI.SetActive(false);
            TCThongShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(thongItem);
            }
        }
        if (Thong3.activeSelf && Thong3Used)
        {
            CollectThong3();
            Thong3Used = false;
            ThongKC.SetActive(true);
            ThongTHC.SetActive(true); 
            ThongTC.SetActive(true);
            KCThongShadowUI.SetActive(false);
            THCThongShadowUI.SetActive(false);
            TCThongShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(thongItem);
            }
        }
        if (Tangpatip1.activeSelf && Tangpatip1Used)
        {
            CollectTangpatip1();
            Tangpatip1Used = false;
            TangpatipKC.SetActive(true);
            TangpatipTHC.SetActive(true);
            TangpatipTC.SetActive(true);
            KCTangpatipShadowUI.SetActive(false);
            THCTangpatipShadowUI.SetActive(false);
            TCTangpatipShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(TangpatipItem);
            }
        }
        if (Tangpatip2.activeSelf && Tangpatip2Used)
        {
            CollectTangpatip2();
            Tangpatip2Used = false;
            TangpatipKC.SetActive(true);
            TangpatipTHC.SetActive(true);
            TangpatipTC.SetActive(true);
            KCTangpatipShadowUI.SetActive(false);
            THCTangpatipShadowUI.SetActive(false);
            TCTangpatipShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(TangpatipItem);
            }
        }
        if (Tangpatip3.activeSelf && Tangpatip3Used)
        {
            CollectTangpatip3();
            Tangpatip3Used = false;
            TangpatipKC.SetActive(true);
            TangpatipTHC.SetActive(true);
            TangpatipTC.SetActive(true);
            KCTangpatipShadowUI.SetActive(false);
            THCTangpatipShadowUI.SetActive(false);
            TCTangpatipShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(TangpatipItem);
            }
        }
        if (Khom1.activeSelf && Khom1Used)
        {
            CollectKhom1();
            Khom1Used = false;
            KhomKC.SetActive(true);
            KhomTHC.SetActive(true);
            KhomTC.SetActive(true);
            KCKhomShadowUI.SetActive(false);
            THCKhomShadowUI.SetActive(false);
            TCKhomShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomItem);
            }
        }
        if (Khom2.activeSelf && Khom2Used)
        {
            CollectKhom2();
            Khom2Used = false;
            KhomKC.SetActive(true);
            KhomTHC.SetActive(true);
            KhomTC.SetActive(true);
            KCKhomShadowUI.SetActive(false);
            THCKhomShadowUI.SetActive(false);
            TCKhomShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomItem);
            }
        }
        if (Khom3.activeSelf && Khom3Used)
        {
            CollectKhom3();
            Khom3Used = false;
            KhomKC.SetActive(true);
            KhomTHC.SetActive(true);
            KhomTC.SetActive(true);
            KCKhomShadowUI.SetActive(false);
            THCKhomShadowUI.SetActive(false);
            TCKhomShadowUI.SetActive(false);
            InventoryController inventoryController = FindObjectOfType<InventoryController>();
            if (inventoryController != null)
            {
                inventoryController.Add(khomItem);
            }
        }
    }
    private void CollectKhomLoi1()
    {
        KhomLoi1.SetActive(false);
    }
    private void CollectKhomLoi2()
    {
        KhomLoi2.SetActive(false);
    }
    private void CollectKhomLoi3()
    {
        KhomLoi3.SetActive(false);
    }
    private void CollectThong1()
    {
        Thong1.SetActive(false);
    }
    private void CollectThong2()
    {
        Thong2.SetActive(false);
    }
    private void CollectThong3()
    {
        Thong3.SetActive(false);
    }
    private void CollectTangpatip1()
    {
        Tangpatip1.SetActive(false);
    }
    private void CollectTangpatip2()
    {
        Tangpatip2.SetActive(false);
    }
    private void CollectTangpatip3()
    {
        Tangpatip3.SetActive(false);
    }
    private void CollectKhom1()
    {
        Khom1.SetActive(false);
    }
    private void CollectKhom2()
    {
        Khom2.SetActive(false);
    }
    private void CollectKhom3()
    {
        Khom3.SetActive(false);
    }
    private void ResetState()
    {
        KhomLoi1Used = true;
        Thong1Used = true;
        Tangpatip1Used = true;
        Khom1Used = true;
        KhomLoi2Used = true;
        Thong2Used = true;
        Tangpatip2Used = true;
        Khom2Used = true;
        KhomLoi3Used = true;
        Thong3Used = true;
        Tangpatip3Used = true;
        Khom3Used = true;
        TangpatipChecker.Instance.ResetState();
        KhomLoiChecker.Instance.ResetState();
        ThongChecker.Instance.ResetState();
    }
}