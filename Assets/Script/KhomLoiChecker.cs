using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class KhomLoiChecker : MonoBehaviour
{
    public GameObject KhomLoi;
    public GameObject Thong;
    public GameObject Tangpatip;
    public GameObject Khom;
    public GameObject KhomLoiKC;
    public GameObject ThongKC;
    public GameObject TangpatipKC;
    public GameObject KhomKC;
    public GameObject KhomLoiTC;
    public GameObject ThongTC;
    public GameObject TangpatipTC;
    public GameObject KhomTC;
    public GameObject KhomLoiTHC;
    public GameObject ThongTHC;
    public GameObject TangpatipTHC;
    public GameObject KhomTHC;
    public GameObject KCKhomLoiShadowUI;
    public GameObject KCThongShadowUI;
    public GameObject KCTangpatipShadowUI;
    public GameObject KCKhomShadowUI;
    public GameObject THCKhomLoiShadowUI;
    public GameObject THCThongShadowUI;
    public GameObject THCTangpatipShadowUI;
    public GameObject THCKhomShadowUI;
    public GameObject TCKhomLoiShadowUI;
    public GameObject TCThongShadowUI;
    public GameObject TCTangpatipShadowUI;
    public GameObject TCKhomShadowUI;
    public GameObject Text;
    private bool KhomLoiUsed = false;
    private bool ThongUsed = false;
    private bool TangpatipUsed = false;
    private bool KhomUsed = false;
    public GameObject uiObject;
    public GameObject Button;
    public static KhomLoiChecker Instance;
    bool isCursorVisible = false;
    void Start()
    {
        KhomLoi.SetActive(false);
        Thong.SetActive(false);
        Tangpatip.SetActive(false);
        Khom.SetActive(false);
        uiObject.SetActive(false);
        Text.SetActive(false);
    }
    private void Awake()
    {
        Instance = this;
    }
    public void InteractWithUI()
    {
        uiObject.SetActive(true);
        Time.timeScale = 0f;
        Button.SetActive(true);
    }
    public void UIOpen()
    {
        uiObject.SetActive(false);
        Time.timeScale = 1f;
        ToggleCursorVisibility();
        Button.SetActive(false);
        Text.SetActive(true);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(!KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
            {
                Text.SetActive(true);
            }
            else if(KhomLoiUsed && ThongUsed && TangpatipUsed && KhomUsed)
            {
                Text.SetActive(false);
            }
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E)  && !KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
            {
                InteractWithUI();
                ToggleCursorVisibility();
                Text.SetActive(false);
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    public void UseKhomLoi()
    {
        if (uiObject.activeSelf && !KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
        {
            KhomLoi.SetActive(true);
            KhomLoiUsed = true;
            UIOpen();
            KhomLoiKC.SetActive(false);
            KhomLoiTC.SetActive(false);
            KhomLoiTHC.SetActive(false);
            KCKhomLoiShadowUI.SetActive(true);
            THCKhomLoiShadowUI.SetActive(true);
            TCKhomLoiShadowUI.SetActive(true);
            Text.SetActive(false);
            Item khomloiItem = null;
            foreach (Item item in InventoryController.Instance.Items)
            {
                if (item.itemType == Item.ItemType.KhomLoi)
                {
                    khomloiItem = item;
                    break;
                }
            }
            if (khomloiItem != null)
            {
                InventoryController.Instance.Remove(khomloiItem);
                InventoryController.Instance.SetInventoryItems();
            }
        }
    }
    public void UseThong()
    {
        if (uiObject.activeSelf && !KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
        {
            Thong.SetActive(true);
            ThongUsed = true;
            UIOpen();
            ThongKC.SetActive(false);
            ThongTC.SetActive(false);
            ThongTHC.SetActive(false);
            KCThongShadowUI.SetActive(true);
            THCThongShadowUI.SetActive(true);
            TCThongShadowUI.SetActive(true);
            Text.SetActive(false);
            Item thongItem = null;
            foreach (Item item in InventoryController.Instance.Items)
            {
                if (item.itemType == Item.ItemType.Thong)
                {
                    thongItem = item;
                    break;
                }
            }
            if (thongItem != null)
            {
                InventoryController.Instance.Remove(thongItem);
                InventoryController.Instance.SetInventoryItems();
            }
        }
    }
    public void UseTangpatip()
    {
        if (uiObject.activeSelf && !KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
        {
            Tangpatip.SetActive(true);
            TangpatipUsed = true;
            UIOpen();
            TangpatipKC.SetActive(false);
            TangpatipTC.SetActive(false);
            TangpatipTHC.SetActive(false);
            KCTangpatipShadowUI.SetActive(true);
            THCTangpatipShadowUI.SetActive(true);
            TCTangpatipShadowUI.SetActive(true);
            Text.SetActive(false);
            Item tangpatipItem = null;
            foreach (Item item in InventoryController.Instance.Items)
            {
                if (item.itemType == Item.ItemType.Tangpatip)
                {
                    tangpatipItem = item;
                    break;
                }
            }
            if (tangpatipItem != null)
            {
                InventoryController.Instance.Remove(tangpatipItem);
                InventoryController.Instance.SetInventoryItems();
            }
        }
    }
    public void UseKhom()
    {
        if (uiObject.activeSelf && !KhomLoiUsed && !ThongUsed && !TangpatipUsed && !KhomUsed)
        {
            Khom.SetActive(true);
            KhomUsed = true;
            UIOpen();
            KhomKC.SetActive(false);
            KhomTC.SetActive(false);
            KhomTHC.SetActive(false);
            KCKhomShadowUI.SetActive(true);
            THCKhomShadowUI.SetActive(true);
            TCKhomShadowUI.SetActive(true);
            Text.SetActive(false);
            Item khomItem = null;
            foreach (Item item in InventoryController.Instance.Items)
            {
                if (item.itemType == Item.ItemType.Khom)
                {
                    khomItem = item;
                    break;
                }
            }
            if (khomItem != null)
            {
                InventoryController.Instance.Remove(khomItem);
                InventoryController.Instance.SetInventoryItems();
            }
        }
    }
    public void ResetState()
    {
        KhomLoiUsed = false;
        ThongUsed = false;
        TangpatipUsed = false;
        KhomUsed = false;
        KhomLoi.SetActive(false);
        Thong.SetActive(false);
        Tangpatip.SetActive(false);
        Khom.SetActive(false);
        uiObject.SetActive(false);
    }
    void ToggleCursorVisibility()
    {
        isCursorVisible = !isCursorVisible;
        if (isCursorVisible)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}