using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameInteract : MonoBehaviour
{
    public GameObject UI;
    public GameObject Text;
    bool isCursorVisible = false;

    void Start()
    {
        UI.SetActive(false);
        Text.SetActive(false);
    }
    void ToggleCursorVisibility()
    {
        isCursorVisible = !isCursorVisible;
        if (isCursorVisible)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                ToggleCursorVisibility();
                Time.timeScale = 0f;
                Text.SetActive(false);
                UI.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
}
