using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
	public GameObject UI;
  	bool isCursorVisible = false;
    void Start()
    {
        
    }
	public void Resume()
	{
		Time.timeScale = 1f;
		UI.SetActive(false);
		ToggleCursorVisibility();
	}
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
			if(!UI.activeSelf)
			{
				InteractWithUI();
            	ToggleCursorVisibility();
        	}
			else if (UI.activeSelf)
			{
				Resume();
			}
    	}
	}
	void ToggleCursorVisibility()
    {
        isCursorVisible = !isCursorVisible;
        if (isCursorVisible)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
	public void InteractWithUI()
	{
		UI.SetActive(true);
        Time.timeScale = 0f;
	}
	public void LoadScene()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
