using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOpen : MonoBehaviour
{
    public static UIOpen Instance;
    public GameObject Khom;
    public GameObject KhomLoi;
    public GameObject Thong;
    public GameObject Tangpatip;
    public GameObject Coin;
    public GameObject Firewood;
    public GameObject UIKhom;
    public GameObject UIKhomLoi;
    public GameObject UIThong;
    public GameObject UITangpatip;
    public GameObject UICoin;
    public GameObject UIFirewood;
    public GameObject Inventory;

    void Awake()
    {
        Instance = this;
    }

    public void KhomOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
		{
			UIKhom.SetActive(true);
            Khom.SetActive(true);
        }
	}
    public void KhomLoiOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
		{
        	UIKhomLoi.SetActive(true);
            KhomLoi.SetActive(true);
    	}
	}
    public void ThongOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
        {
        	UIThong.SetActive(true);
            Thong.SetActive(true);
    	}
	}
    public void TangpatipOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
        {
        	UITangpatip.SetActive(true);
            Tangpatip.SetActive(true);
    	}
	}
    public void CoinOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
		{
        	UICoin.SetActive(true);
            Coin.SetActive(true);
    	}
	}
    public void FirewoodOpen()
    {
		if(!UIKhom.activeSelf && !UIKhomLoi.activeSelf && !UIThong.activeSelf && !UITangpatip.activeSelf && !UICoin.activeSelf && !UIFirewood.activeSelf)
		{
        	UIFirewood.SetActive(true);
            Firewood.SetActive(true);
    	}
	}
    void Update()
    {
        if(Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.F))
        {
            if (Inventory.activeSelf)
            {
                UIKhom.SetActive(false);
                UIKhomLoi.SetActive(false);
                UIThong.SetActive(false);
                UITangpatip.SetActive(false);
                UICoin.SetActive(false);
                UIFirewood.SetActive(false);
                Khom.SetActive(false);
                KhomLoi.SetActive(false);
                Thong.SetActive(false);
                Tangpatip.SetActive(false);
                Coin.SetActive(false);
                Firewood.SetActive(false);
            }
        }
    }
}
