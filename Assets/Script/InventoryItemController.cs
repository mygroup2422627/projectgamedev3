using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemController : MonoBehaviour
{
    public static InventoryItemController Instance;
    public Item item;
    private void Awake()
    {
        Instance = this;
    }
    public void RemoveItem()
    {
        InventoryController.Instance.Remove(item);
        Destroy(gameObject);
    }
	public void OpenKhomLoi()
	{
		UIOpen.Instance.KhomLoiOpen();
	}
	public void OpenKhom()
    {
		UIOpen.Instance.KhomOpen();
	}
	public void OpenThong()
	{
        UIOpen.Instance.ThongOpen();
	}
	public void OpenTangpatip()
	{
		UIOpen.Instance.TangpatipOpen();
	}
	public void OpenFirewood()
	{
		UIOpen.Instance.FirewoodOpen();
	}
	public void OpenCoin()
	{
		UIOpen.Instance.CoinOpen();
	}
    public void AddItem(Item newItem)
    {
        item = newItem;
    }
 	public void OpenItem()
    {
        switch(item.itemType)
        {
            case Item.ItemType.Khom:
                OpenKhom();
                break;
            case Item.ItemType.KhomLoi:
                OpenKhomLoi();
                break;
            case Item.ItemType.Thong:
                OpenThong();
                break;
            case Item.ItemType.Tangpatip:
                OpenTangpatip();
                break;
            case Item.ItemType.Coin:
                OpenCoin();
                break;
            case Item.ItemType.Firewood:
                OpenFirewood();
                break;
        }
    }
}