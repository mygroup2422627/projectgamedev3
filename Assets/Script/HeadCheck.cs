using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadCheck : MonoBehaviour
{
    public GameObject GoldBar;
    public GameObject Lotus;
    public GameObject Head;
    public GameObject HeadOnPuzzle;
    public GameObject Text;
    private bool GoldBarActive = false;
    private bool LotusActive = false;
    private bool HeadActive = false;
    public static HeadCheck Instance;

    private void Awake()
    {
        Instance = this;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!Head.activeSelf)
            {
                Text.SetActive(true);
            }
            else if (Head.activeSelf)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                if (!GoldBarActive && !LotusActive && !HeadActive)
                {
                    Head.SetActive(true);
                    HeadActive = true;
                    HeadOnPuzzle.SetActive(false);
                    Text.SetActive(false);
                }
            }
        }
        if (Lotus.activeSelf)
        {
            LotusActive = true;
        }
        if (GoldBar.activeSelf)
        {
            GoldBarActive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    public void Reset()
    {
        GoldBarActive = false;
        LotusActive = false;
        HeadActive = false;
    }
}