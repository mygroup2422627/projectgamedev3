using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftOpenTrigger : MonoBehaviour
{
    public GameObject LeftLift;
    public GameObject RightLift;
    private Animation LeftLiftAnimation;
    private Animation RightLiftAnimation;
    private bool animationPlayed = false;
    void Start()
    {
        LeftLiftAnimation = LeftLift.GetComponent<Animation>();
        RightLiftAnimation = RightLift.GetComponent<Animation>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !animationPlayed)
        {
            LeftLiftAnimation.Play("LeftLift");
            RightLiftAnimation.Play("RightLift");
            animationPlayed = true;
        }
    }
}