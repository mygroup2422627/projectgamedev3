using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotusCheck : MonoBehaviour
{
    public GameObject GoldBar;
    public GameObject Lotus;
    public GameObject Head;
    public GameObject LotusOnPuzzle;
    public GameObject Text;
    private bool GoldBarActive = false;
    private bool LotusActive = false;
    private bool HeadActive = false;
    public static LotusCheck Instance;

    private void Awake()
    {
        Instance = this;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!Lotus.activeSelf)
            {
                Text.SetActive(true);
            }
            else if (Lotus.activeSelf)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                if (!GoldBarActive && !LotusActive && !HeadActive)
                {
                    Lotus.SetActive(true);
                    LotusActive = true;
                    LotusOnPuzzle.SetActive(false);
                    Text.SetActive(false);
                }
            }
        }
        if (GoldBar.activeSelf)
        {
            GoldBarActive = true;
        }
        if (Head.activeSelf)
        {
            HeadActive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    public void Reset()
    {
        GoldBarActive = false;
        LotusActive = false;
        HeadActive = false;
    }
}