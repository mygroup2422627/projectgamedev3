using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryController : MonoBehaviour
{
	public static InventoryController Instance;
    public List<Item> Items = new List<Item>();
    public GameObject uiObject;
    public Transform ItemContent;
    public GameObject InventoryItem;
    public InventoryItemController[] InventoryItems;
    bool isCursorVisible = false;
    public GameObject Icon;
    private Animation InventoryAnimation;
    bool InPlayed = false;
	private bool isPlayingInventoryAnimation = false;
    void Start()
    {
		InventoryAnimation = uiObject.GetComponent<Animation>();
		uiObject.SetActive(false);
        Icon.SetActive(true);
    }
	private void Awake()
    {
		Instance = this;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && !isPlayingInventoryAnimation)
        {
            InteractWithUI();
        }
    }
    void ToggleCursorVisibility()
    {
        isCursorVisible = !isCursorVisible;
        if (isCursorVisible)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 0f;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1f;
        }
    }
    public void InteractWithUI()
	{
		if (uiObject.activeSelf)
    	{
          
            StartCoroutine(DeactivateUIAfterAnimation());
    	}
    	else
    	{
        	StartCoroutine(ActivateUIAfterAnimation());
        }
	}
	private IEnumerator DeactivateUIAfterAnimation()
    {
        isPlayingInventoryAnimation = true;
        ToggleCursorVisibility();
        InventoryAnimation.Play("InventoryOut");
        yield return new WaitForSeconds(1f);
        uiObject.SetActive(false);
        Icon.SetActive(true);
        isPlayingInventoryAnimation = false;
    }

    private IEnumerator ActivateUIAfterAnimation()
    {
        Icon.SetActive(false);
        isPlayingInventoryAnimation = true;
        uiObject.SetActive(true);
        InventoryAnimation.Play("InventoryIn");
        ListItems();
        yield return new WaitForSeconds(1f);
        ToggleCursorVisibility();
        isPlayingInventoryAnimation = false;
    }
    public void Add(Item item)
    {
        Items.Add(item);
    }
    public void Remove(Item item)
    {
        Items.Remove(item);
    }
    public void ListItems()
	{
    	for (int i = 0; i < InventoryItems.Length; i++)
    	{
        	Destroy(InventoryItems[i].gameObject);
    	}
    	InventoryItems = new InventoryItemController[Items.Count];
        for (int i = 0; i < Items.Count; i++)
    	{
        	GameObject obj = Instantiate(InventoryItem, ItemContent);
        	InventoryItemController itemController = obj.GetComponent<InventoryItemController>();
        	InventoryItems[i] = itemController;
        	var itemName = obj.transform.Find("ItemName").GetComponent<TextMeshProUGUI>();
        	var itemIcon = obj.transform.Find("ItemIcon").GetComponent<Image>();
    	    itemName.text = Items[i].itemName;
        	itemIcon.sprite = Items[i].icon;
        	InventoryItems[i].AddItem(Items[i]);
    	}
	}
    public void SetInventoryItems()
    {
	    InventoryItems = ItemContent.GetComponentsInChildren<InventoryItemController>();

	    for (int i = 0; i < InventoryItems.Length; i++)
	    {
		    if (i < Items.Count)
		    {
			    InventoryItems[i].AddItem(Items[i]);
		    }
	    }
    }

}