using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    protected Animator m_Animator;
    public Transform Position;
	private static readonly int Forward = Animator.StringToHash("Forward");
    private void Start()
	{
		m_Animator = GetComponent<Animator>();
	}
	void Update()
	{
		if(Input.GetKey(KeyCode.W)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.D))
		{
			m_Animator.SetFloat(Forward,0.5f);
		}
		else
		{
			m_Animator.SetFloat(Forward,0.0f);
		}
	}
}
