using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hint : MonoBehaviour
{
    public GameObject uiObject;
    public GameObject Text;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag =="Player")
        {
            if(!uiObject.activeSelf)
            {
                Text.SetActive(true);
            }
            else if(uiObject.activeSelf)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !uiObject.activeSelf)
            {
                Text.SetActive(false);
                ToggleCursorVisibility();
                Time.timeScale = 0f;
                InteractWithUI();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
        }
    }
    private void Start()
    {
        uiObject.SetActive(false);
    }
    void ToggleCursorVisibility()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
    }
    public void InteractWithUI()
    {
        uiObject.SetActive(true);
    }
    public void UIOpen()
    {
        uiObject.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        Text.SetActive(true);
    }
}
