using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle4Coin : MonoBehaviour
{
    public GameObject Spotlight1;
    public GameObject Spotlight2;
    public GameObject Spotlight3;
    public GameObject Spotlight4;
    public GameObject Coin;
    void Update()
    {
        bool isSpotlight1Active = Spotlight1.activeSelf;
        bool isSpotlight2Active = Spotlight2.activeSelf;
        bool isSpotlight3Active = Spotlight3.activeSelf;
        bool isSpotlight4Active = Spotlight4.activeSelf;
        if (isSpotlight1Active && !isSpotlight2Active && !isSpotlight3Active && !isSpotlight4Active)
        {
            if (Coin != null)
            {
                Coin.SetActive(true);
            }
        }
        if (isSpotlight2Active && !isSpotlight1Active && !isSpotlight3Active && !isSpotlight4Active)
        {
            if (Coin != null)
            {
                Coin.SetActive(true);
            }
        }
    }
}