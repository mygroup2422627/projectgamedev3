using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    public GameObject option;
    public GameObject howtoplay;
    public GameObject exit;

    public void GameStart()
    {
        option.SetActive(false);
        howtoplay.SetActive(false);
        exit.SetActive(false);
    }
}
