using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private float walkSpeed = 50f;
    private Vector2 moveInput;
    private Rigidbody myRigidbody;
    [SerializeField] private int Stamina = 60;
	public int maximumStamina = 60;
    private bool isRunning = false;
    private int increaseStaminaAmount = 3;
    public GameObject Tired;
    public GameObject Tired2;
    public GameObject Tired3;
    public GameObject Tired4;
    [SerializeField] private Slider StaSlider;
    private Coroutine increaseStaminaCoroutineRef;
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        Collider playerCollider = gameObject.GetComponent<Collider>();
        playerCollider.isTrigger = true;
    }
    void Update()
    {
        Run();
        StaSlider.value = Stamina;
        if (Stamina >= 0 && Stamina < 6)
        {
            Tired.SetActive(true);
            Tired2.SetActive(false);
            Tired3.SetActive(false);
            Tired4.SetActive(false);
        }
        else if (Stamina >= 6 && Stamina < 12)
        {
            Tired.SetActive(false);
            Tired2.SetActive(true);
            Tired3.SetActive(false);
            Tired4.SetActive(false);
        }
        else if (Stamina >= 12 && Stamina < 18)
        {
            Tired.SetActive(false);
            Tired2.SetActive(false);
            Tired3.SetActive(true);
            Tired4.SetActive(false);
        }
        else if (Stamina >= 18 && Stamina < 24)
        {
            Tired.SetActive(false);
            Tired2.SetActive(false);
            Tired3.SetActive(false);
            Tired4.SetActive(true);
        }
        else if (Stamina >= 24)
        {
            Tired.SetActive(false);
            Tired2.SetActive(false);
            Tired3.SetActive(false);
            Tired4.SetActive(false);
        }
    }
    void Run()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (!isRunning && Stamina >= 20)
            {
                StartCoroutine(RunCoroutine());

            }

            StopIncreaseStaminaCoroutine();
        }
        else
        {
            StopRunning();
            if (Stamina < maximumStamina && !isRunning)
            {
                StartIncreaseStaminaCoroutine();
            }
        }
        Vector3 playerVelocity = new Vector3(moveInput.x * walkSpeed, myRigidbody.velocity.y, moveInput.y * walkSpeed);
        myRigidbody.velocity = transform.TransformDirection(playerVelocity);
    }
    public void OnMove(InputValue value)
    {
        moveInput = value.Get<Vector2>();
    }

    IEnumerator RunCoroutine()
    {
        isRunning = true;
        walkSpeed = 100;
        while (Stamina >= 20 && Input.GetKey(KeyCode.LeftShift))
        {
            Stamina -= 20;
            yield return new WaitForSeconds(1f);
        }
        StopRunning();
    }
    void StopRunning()
    {
        isRunning = false;
        walkSpeed = 50f;
    }
    void StartIncreaseStaminaCoroutine()
    {
        if (increaseStaminaCoroutineRef == null)
        {
            increaseStaminaCoroutineRef = StartCoroutine(IncreaseStaminaCoroutine());
        }
    }
    void StopIncreaseStaminaCoroutine()
    {
        if (increaseStaminaCoroutineRef != null)
        {
            StopCoroutine(increaseStaminaCoroutineRef);
            increaseStaminaCoroutineRef = null;
        }
    }
    IEnumerator IncreaseStaminaCoroutine()
    {
        while (Stamina < maximumStamina)
        {
            Stamina += increaseStaminaAmount;
            if (Stamina > maximumStamina)
                Stamina = maximumStamina;
            yield return new WaitForSeconds(1f);
        }
    }
}