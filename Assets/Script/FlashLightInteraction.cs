using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightInteraction : MonoBehaviour
{
    public GameObject FlashLight;
    public GameObject FlashLightPickup;
    public GameObject SpotLight;
    public GameObject FL;
    public GameObject Text;
    void Start()
    {
        SpotLight.SetActive(false);
        FlashLight.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player2")
        {
            if(!FlashLight.activeSelf)
            {
                Text.SetActive(true);
            }
            else if (FlashLight.activeSelf)
            {
                Text.SetActive(false);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player2")
        {
            if (Input.GetKey(KeyCode.E))
            {
                SpotLight.SetActive(true);
                FlashLight.SetActive(true);
                FlashLightPickup.SetActive(false);
                Text.SetActive(false);
                FL.SetActive(false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player" || other.gameObject.tag == "Player2")
        {
            Text.SetActive(false);
        }
    }
}
