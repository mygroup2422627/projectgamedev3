using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour
{
    public static Reset Instance;
	void Awake()
	{
		Instance = this;
	}
	public void ResetGame()
	{
		Time.timeScale = 1f;
	}
}