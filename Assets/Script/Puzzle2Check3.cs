using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle2Check3 : MonoBehaviour
{
    public GameObject Gold;
    public GameObject Lotus;
    public GameObject Head;
    public GameObject GoldOnPlayer;
    public GameObject LotusOnPlayer;
    public GameObject HeadOnPlayer;
    private bool GoldUsed = false;
    private bool LotusUsed = false;
    private bool HeadUsed = false;
    public GameObject Text;
    public GameObject TextQ;
    void Start()
    {
        Gold.SetActive(false);
        Lotus.SetActive(false);
        Head.SetActive(false);
        Text.SetActive(false);
        TextQ.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (GoldUsed || LotusUsed || HeadUsed)
            {
                Text.SetActive(false);
                TextQ.SetActive(true);
            }
            else if(GoldOnPlayer.activeSelf || LotusOnPlayer.activeSelf || HeadOnPlayer.activeSelf)
            {
                Text.SetActive(true);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                if (GoldOnPlayer.activeSelf && !LotusUsed && !HeadUsed)
                {
                    Gold.SetActive(true);
                    GoldUsed = true;
                    GoldOnPlayer.SetActive(false);
                    Text.SetActive(false);
                    TextQ.SetActive(true);
                }
                if (LotusOnPlayer.activeSelf && !GoldUsed && !HeadUsed)
                {
                    Lotus.SetActive(true);
                    LotusUsed = true;
                    LotusOnPlayer.SetActive(false);
                    Text.SetActive(false);
                    TextQ.SetActive(true);
                }
                if (HeadOnPlayer.activeSelf && !GoldUsed && !LotusUsed)
                {
                    Head.SetActive(true);
                    HeadUsed = true;
                    HeadOnPlayer.SetActive(false);
                    Text.SetActive(false);
                    TextQ.SetActive(true);
                }
            }
            if (Input.GetKey(KeyCode.Q))
            {
                if (Gold.activeSelf && !LotusOnPlayer.activeSelf && !HeadOnPlayer.activeSelf)
                {
                    Gold.SetActive(false);
                    GoldUsed = false;
                    GoldOnPlayer.SetActive(true);
                    Text.SetActive(true);
                    TextQ.SetActive(false);
                }
                if (Lotus.activeSelf && !GoldOnPlayer.activeSelf && !HeadOnPlayer.activeSelf)
                {

                    Lotus.SetActive(false);
                    LotusUsed = false;
                    LotusOnPlayer.SetActive(true);
                    Text.SetActive(true);
                    TextQ.SetActive(false);
                }
                if (Head.activeSelf && !GoldOnPlayer.activeSelf && !LotusOnPlayer.activeSelf)
                {
                    Head.SetActive(false);
                    HeadUsed = false;
                    HeadOnPlayer.SetActive(true);
                    Text.SetActive(true);
                    TextQ.SetActive(false);
                }
                GoldBarCheck.Instance.Reset();
                LotusCheck.Instance.Reset();
                HeadCheck.Instance.Reset();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Text.SetActive(false);
            TextQ.SetActive(false);
        }
    }
}