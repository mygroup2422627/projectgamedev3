using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldBarCheck : MonoBehaviour
{
	public GameObject GoldBar;
	public GameObject Lotus;
	public GameObject Head;
	public GameObject GoldBarOnPuzzle;
	public GameObject Text;
	private bool GoldBarActive = false;
	private bool LotusActive = false;
	private bool HeadActive = false;
	public static GoldBarCheck Instance;

	private void Awake()
	{
		Instance = this;
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (!GoldBar.activeSelf)
			{
				Text.SetActive(true);
			}
			else if (GoldBar.activeSelf)
			{
				Text.SetActive(false);
			}
		}
	}
	private void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (Input.GetKey(KeyCode.E))
			{
				if (!GoldBarActive && !LotusActive && !HeadActive)
				{
					GoldBar.SetActive(true);
					GoldBarActive = true;
					GoldBarOnPuzzle.SetActive(false);
					Text.SetActive(false);
				}
			}
		}
		if (Lotus.activeSelf)
		{
			LotusActive = true;
		}
		if (Head.activeSelf)
		{
			HeadActive = true;
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
        {
			Text.SetActive(false);
        }
	}

	public void Reset()
	{
		GoldBarActive = false;
		LotusActive = false;
		HeadActive = false;
	}
}