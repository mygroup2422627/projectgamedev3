using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public Item Item;
    private bool canPickup = true;
    public static ItemPickup Instance;
    public GameObject EText;

    void Start()
    {
        EText.SetActive(false);
    }
    public void Pickup()
    {
        InventoryController.Instance.Add(Item);
        Destroy(gameObject);
    }
    private void Awake()
    {
        Instance = this;
    }
    private IEnumerator WaitAndEnablePickup()
    {
        canPickup = false;
        yield return new WaitForSeconds(0.2f);
        canPickup = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && canPickup)
        {
            EText.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                EText.SetActive(false);
                Pickup();
                StartCoroutine(WaitAndEnablePickup());
            }
        }
    }

    private void OnTriggerExit()
    {
        EText.SetActive(false);
    }
}