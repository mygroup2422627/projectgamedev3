using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public GameObject LeftLift;
    public GameObject RightLift;
    public GameObject Elevator;
    private Animation LeftLiftAnimation;
    private Animation RightLiftAnimation;
    private bool animationOpen = false;
    void Start()
    {
        LeftLiftAnimation = LeftLift.GetComponent<Animation>();
        RightLiftAnimation = RightLift.GetComponent<Animation>();
    }
    void Update()
    {
        if (!animationOpen)
        {
            animationOpen = true;
            StartCoroutine(WaittoStart());
        }
    }
    private IEnumerator WaittoStart()
    {
        yield return new WaitForSeconds(1.5f);
        EnableAudio(Elevator);
        LeftLiftAnimation.Play("LeftLiftOpen");
        RightLiftAnimation.Play("RightLiftOpen");
    }
    private void EnableAudio(GameObject obj)
    {
        AudioSource audio = obj.GetComponent<AudioSource>();
        if (audio != null)
        {
            audio.enabled = true;
        }
    }
}
