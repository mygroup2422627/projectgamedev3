using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour
{
    public AudioClip soundToPlay; 
    private AudioSource audioSource;
    private bool hasPlayed = false;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = soundToPlay;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && !hasPlayed)
        {
            audioSource.Play();
            hasPlayed = true;
        }
    }
}
