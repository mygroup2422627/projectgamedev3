using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextCheck : MonoBehaviour
{
    public GameObject Text;
    
    private void Start()
    {
        StartCoroutine(ActivateAndDeactivateText(3f));
    }

    private IEnumerator ActivateAndDeactivateText(float delay)
    {
        yield return new WaitForSeconds(delay);
        Text.SetActive(false);
    }
}