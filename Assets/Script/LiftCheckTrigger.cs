using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftCheckTrigger : MonoBehaviour
{
    public GameObject LeftLift;
    public GameObject RightLift;
    private Animation LeftLiftAnimation;
    private Animation RightLiftAnimation;
    public GameObject Enemy;
    private bool animationPlayed = false;
    private bool animationOpenPlayed = false;
    public GameObject Fire1;
    public GameObject Fire2;
    public GameObject Fire3;
    public GameObject Fire4;
    public GameObject Lift;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;
    public GameObject Enemy4;
    public GameObject TeePorTeeMae;
    public GameObject Elevator;
    public GameObject BGM;
    void Start()
    {
        LeftLiftAnimation = LeftLift.GetComponent<Animation>();
        RightLiftAnimation = RightLift.GetComponent<Animation>();
        Lift.SetActive(false);
        Enemy1.SetActive(false);
        Enemy2.SetActive(false);
        Enemy3.SetActive(false);
        Enemy4.SetActive(false);
        TeePorTeeMae.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !animationPlayed)
        {
            LeftLiftAnimation.Play("LeftLift");
            RightLiftAnimation.Play("RightLift");
            animationPlayed = true;
            Enemy.SetActive(true);
            EnableScript(Enemy);
        }
    }
    void Update()
    {
        if (Fire1.activeSelf && Fire2.activeSelf && Fire3.activeSelf && Fire4.activeSelf && !animationOpenPlayed)
        {
            LeftLiftAnimation.Play("LeftLiftOpen");
            RightLiftAnimation.Play("RightLiftOpen");
            animationOpenPlayed = true;
            Lift.SetActive(true);
            Enemy1.SetActive(true);
            Enemy2.SetActive(true);
            Enemy3.SetActive(true);
            Enemy4.SetActive(true);
            TeePorTeeMae.SetActive(true);
            EnableSound(Elevator);
            DisableBGM(BGM);
        }
    }
    private void EnableScript(GameObject obj)
    {
        EnemyFollowPlayer enemy = obj.GetComponent<EnemyFollowPlayer>();
        EnemyAnimation enemyanim = obj.GetComponent<EnemyAnimation>();
        Collider collider = obj.GetComponent<CapsuleCollider>();
        AudioSource audio = obj.GetComponent<AudioSource>();
        if (enemy != null && enemyanim != null && collider != null && audio != null)
        {
            enemy.enabled = true;
            enemyanim.enabled = true;
            collider.enabled = true;
            audio.enabled = true;
        }
    }

    private void EnableSound(GameObject obj)
    {
        AudioSource elevator = obj.GetComponent<AudioSource>();
        if (elevator != null)
        {
            elevator.enabled = true;
        }
    }

    private void DisableBGM(GameObject obj)
    {
        AudioSource bgm = obj.GetComponent<AudioSource>();
        if (bgm != null)
        {
            bgm.enabled = false;
        }
    }
}